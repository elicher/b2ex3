import React from 'react';

import Items from './Items';

const List = (props) => (
  <ul>
    {
		props.categories.map((ele, i) => <Items item={ele} />)
		}
  </ul>
);

export default List;
